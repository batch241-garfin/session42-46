const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, nexts) => {

	let token = req.headers.authorization;

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return res.send({auth: "failed"});
			}
			else
			{
				nexts();
			}
		});
	}
	else
	{
		return res.send({auth: "failed"});
	}
}

module.exports.decode = (token) => {

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return null;
			}
			else
			{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else
	{
		return null;
	}
}