
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for CREATING A PRODUCT (ADMIN Only)

router.post("/add", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));	
	}
});


// Route for RETRIEVING ALL THE ACTIVE PRODUCTS

router.get("/activeProducts", (req,res) => {

	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});


// Route for RETRIEVING SPECIFIC PRODUCT

router.get("/:productId/getProduct", (req,res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for UPDATING A PRODUCT (ADMIN ONLY)

router.put("/:productId/updateProduct", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));	
	}
});


// Route for ARCHIVING A PRODUCT

router.patch("/:productId/archive", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
	
});


// Route for RETRIEVING ALL THE PRODUCTS BY ADMIN

router.get("/allProducts", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		productController.AllProducts(req.params).then(resultFromController => res.send(resultFromController));
	};

	
});

// Route for UPDATING THE PRODUCT INTO ACTIVE/NOT ACTIVE

router.patch("/:productId/updateActive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(req.params)

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		productController.activeProducts(req.params, req.body).then(resultFromController => res.send(resultFromController));
	};
})


// Route for SEARCHING PRODUCTS
router.post("/search", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
	productController.searchInput(req.body).then(resultFromController => res.send(resultFromController));
	}
})




module.exports = router;