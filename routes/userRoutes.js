
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for USER REGISTRATION

router.post("/register", (req,res) => {

	userController.userRegister(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for USER AUTHENTICATION

router.post("/login", (req,res) => {

	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for CREATE ORDER (NON-ADMIN)

router.post("/createOrder", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true)
	{
		res.send(false);
	}
	else
	{
		let data = {
		 orderId: auth.decode(req.headers.authorization).id,
		 prodId: req.body.productId,
		 quantity: req.body.quantity
		};

		userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	};
});


// Route for USER CHECKOUT (NON-ADMIN)

router.get("/:userId/checkOut", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true)
	{
		res.send(false);
	}
	else
	{
		userController.checkOut(req.params).then(resultFromController => res.send(resultFromController));
	};
});


// Route for RETRIEVING ALL THE USERS

router.get("/all", (req,res) => {

	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});


// Route for RETRIEVING THE USER BY ITS ID

router.get("/:userId/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.userDetails(req.params).then(resultFromController => res.send(resultFromController));
})


// Route for SETTING USER AS ADMIN

router.patch("/:userId/asAdmin", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		userController.userAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}
	
});


// Route for RETRIEVING USER'S ORDER

router.get("/userOrder", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == true)
	{
		res.send(false);
	}
	else
	{
		userController.userOrder({id:userData.id}).then(resultFromController => res.send(resultFromController));
	};
});


// Route for RETRIEVING ALL ORDERS

router.get("/allOrders", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin == false)
	{
		res.send("Unauthorized User");
	}
	else
	{
		userController.getAllOrders(req.params).then(resultFromController => res.send(resultFromController));
	};

	
});


// Retreive user details (For Frontend)
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


module.exports = router;