
const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");


// Module for CREATING A PRODUCT
// moonstone 50 Blocks out the sun and creates an artificial night.
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product
	({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {

		if (error) {

			return false;
		}
		else
		{
			return true;
		};
	});
};


// Module for RETRIEVING ALL ACTIVE PRODUCTS

module.exports.getAllActiveProducts = () => {

	return Product.find({isActive: true}).then(result => {
		return result;
	});
};


// Module for RETRIEVING SPECIFIC PRODUCT

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};


// Module for UPDATING A SPECIFIC PRODUCT

module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	console.log(updateProduct)

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {

		if (error) {
			return false;
		}
		else
		{
			return true;
		};
	});
};


// Module for ARCHIVING A COURSE

module.exports.archiveProduct = (reqParams, reqBody) => {

	let patchProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, patchProduct).then((product, error) => {

		if (error) {
			return false;
		}
		else
		{
			return "Item Archived";
		};
	});
};


// Module for RETRIEVING ALL PRODUCTS

module.exports.AllProducts = () => {

	return Product.find({}).then(result => {
		return result;
	});
};


// Module for UPDATING THE PRODUCTS INTO ACTIVE/NOT ACTIVE

module.exports.activeProducts = (reqParams, reqBody) => {

	let updateActive = {
		isActive: reqBody.isActive
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActive).then((product, error) => {

		if (error) {
			return false;
		}
		else
		{
			return true;
		};
	});
};


// Module for SEARCHING PRODUCT
module.exports.searchInput = (reqBody) => {

	return Product.find({name:reqBody.name}).then(result => {		
		return result
	})
}