
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Module for USER REGISTRATION

module.exports.userRegister = (reqBody) => {

	return User.findOne({email:reqBody.email}).then(result => {
		if (result == null) {

			let regUser = new User({

				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
				});

				return regUser.save().then((user,error) => {

					if (error) {

						return false;
					}
					else
					{
						return user;
					};
				});
		}

		else
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return false
				//"User already exists"
			}
			else
			{
				return false
				//"Email already exists"
			}
		}
	});
};


// Module for USER AUTHENTICATION

module.exports.userLogin = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if (result == null) {
			return false;
		}
		else
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return { access: auth.createAccessToken(result)};
			}
			else
			{
				return false;
			};
		};
	});
};


// Module for CREATE ORDER (NON-ADMIN)

module.exports.createOrder = async (data) => {

	let subtotal;
	let prodName;

	Product.findById(data.prodId).then(result => {

		subtotal = result.price * data.quantity;
		prodName = result.name;
		
		});

	let isUserUpdated = await User.findById(data.orderId).then(user => {

		user.ordersU.push({products: [{
			prodId: data.prodId,
			productName: prodName,
			quantity: data.quantity
		}],totalAmount: subtotal});

		
		return user.save().then((user, error) => {

			if (error) {
				return false;
			}
			else
			{
				return true;
			}
		});
	});


	let isProductUpdated = await Product.findById(data.prodId).then(product => {

		product.ordersP.push({orderId: data.orderId});

		return product.save().then((product, error) => {

			if (error) {
				return false;
			}
			else
			{
				return true;
			}
		});
	});

	if (isUserUpdated && isProductUpdated) {
		return true;
	}
	else
	{
		return false;
	};
}


// Module for USER CHECKOUT

module.exports.checkOut = (reqParams) => {

	let sum = 0;
	return User.findById(reqParams.userId).then(result => {

		for(let x = 0; x < result.ordersU.length; x++)
		{
			let y = result.ordersU[x].totalAmount;
			sum = sum + y
		}
			return `${sum}`
	});
};


// Module for RETRIEVING ALL USERS

module.exports.getAllUsers = () => {

	return User.find({}).then(result => {
		return result;
	});
};


// Module for RETRIEVING USER DETAILS

module.exports.userDetails = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {

		if (result == null) {
			return false;
		}
		else
		{
			return result;
		}
	})
}


// Module for SETTING USER AS ADMIN

module.exports.userAsAdmin = (reqParams, reqBody) => {

	let userAdmin = {
		isAdmin: true
	};

	return User.findByIdAndUpdate(reqParams.userId, userAdmin).then((admin, error) => {

		if (error) {
			return false;
		}
		else
		{
			return "User changed into admin";
		};
	});
};


// Module for RETRIEVING USER'S ORDER

module.exports.userOrder = (reqParams) => {

	return User.findById(reqParams.id).then(result => {

		return result.ordersU
	});
};


// Module for RETRIEVING ALL ORDERS

module.exports.getAllOrders = (reqBody) => {

	let arr = []
	return User.find({}).then(result => {

		for(let x = 0; x < result.length; x++)
		{
				arr.push(result[x].ordersU)
		}
		return arr
	});
};


// Retreive user details (For Frontend)
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};